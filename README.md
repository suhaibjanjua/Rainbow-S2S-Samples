![Rainbow](images/logo_rainbow.png)

# Rainbow S2S (Server to Server) Starter Kit

## Rainbow-S2S-Samples

---

Applications samples to show how to use  S2S API

- [Rainbow-S2S-Sample-CountryBot](https://github.com/Rainbow-CPaaS/Rainbow-S2S-Samples/tree/master/Rainbow-S2S-Sample-CountryBot) : Demonstrate how to create a bot that user can use to request country informations comming from a third party web service.

